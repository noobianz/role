# == Class: role::webserver::server
# Role webserver
#
# === Parameters
#
# None
#
# === Variables
#
# None
#
# === Examples
#
#  include role::webserver::server
#
# === Authors
#
# Rodney Brown <rodney@punchtech.com>
#
# === Copyright
#
# Copyright 2015 Rodney Brown
#
class role::webserver::server {
  include profile::base 
  include profile::common::apache 
}

