# == Class: role::devserver::server
# Role wms
#
# === Parameters
#
# None
#
# === Variables
#
# None
#
# === Examples
#
#  include role::devserver::server
#
# === Authors
#
# Rodney Brown <rodney@punchtech.com>
#
# === Copyright
#
# Copyright 2015 Rodney Brown
#
class role::devserver::server {
  include profile::base 
#  include profile::common::printers 
#  include profile::common::users 
#  include profile::devserver::users
}

