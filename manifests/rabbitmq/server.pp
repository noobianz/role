# == Class: role::rabbitmq::server
# Role rabbitmq
#
# === Parameters
#
# None
#
# === Variables
#
# None
#
# === Examples
#
#  include role::rabbitmq::server
#
# === Authors
#
# Rodney Brown <rodney@punchtech.com>
#
# === Copyright
#
# Copyright 2015 Rodney Brown
#
class role::rabbitmq::server {
  include profile::base
  include profile::rabbitmq::users
  # include profile::common::epel
  include profile::common::erlang
  include profile::rabbitmq::rabbitmq
}

